import helper
import re


def create_lookup_tables(input_data):
    vocab = set(input_data)

    # 文字到數字的映射
    vocab_to_int = {word: idx for idx, word in enumerate(vocab)}

    # 數字到文字的映射
    int_to_vocab = dict(enumerate(vocab))

    return vocab_to_int, int_to_vocab


def token_lookup():
    symbols = set(['。', '，', '“', "”", '；', '！', '？', '（', '）', '——', '\n'])

    tokens = ["P", "C", "Q", "T", "S", "E", "M", "I", "O", "D", "R"]

    return dict(zip(symbols, tokens))

dir = './等一個人咖啡.txt'
text = helper.load_text(dir)

num_words_for_training = 136000

text = text[:num_words_for_training]

lines_of_text = text.split('\n')

print(len(lines_of_text))

print(lines_of_text[:20])

# 去空行
lines_of_text = [lines for lines in lines_of_text if len(lines) > 0]
print(len(lines_of_text))

print(lines_of_text[:20])

# 去掉每行首尾空格
lines_of_text = [lines.strip() for lines in lines_of_text]
print(len(lines_of_text))

print(lines_of_text[:20])

# Regular expression
pattern = re.compile(r'\[.*\]')
lines_of_text = [pattern.sub("", lines) for lines in lines_of_text]
print(len(lines_of_text))
print(lines_of_text[:20])

print(lines_of_text[-20:])

helper.preprocess_and_save_data(''.join(lines_of_text), token_lookup, create_lookup_tables)

int_text, vocab_to_int, int_to_vocab, token_dict = helper.load_preprocess()
